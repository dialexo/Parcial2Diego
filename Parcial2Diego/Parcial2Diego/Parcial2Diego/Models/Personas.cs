﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parcial2Diego.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]

        public int cedula { get; set; }

        public string nombres { get; set; }

        public string apellidos { get; set; }

        public DateTime fechaNa { get; set; }


    }
}
