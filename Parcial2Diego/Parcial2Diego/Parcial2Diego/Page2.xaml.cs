﻿using Parcial2Diego.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Parcial2Diego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}


        public void buscarP()
        {
            int Cedula = int.Parse(entrynumero.Text);
            using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
            {
                var listPer = conecction.Table<Personas>();
                string n = null;
                foreach (var aux in listPer)
                {
                    if (aux.cedula == Cedula)
                    {
                        nombres.Text = aux.nombres.ToString();
                        apellidos.Text = aux.apellidos.ToString();
                        fecha.Text = aux.fechaNa.ToString();
                        n = "encontrado";
                    }
                }
                if (n == null)
                {
                    DisplayAlert("Advertencia", "El usuario no fue encontrado", "ok");
                }
            }
            entrynumero.Text = "";
        }

        public void eliminarP()
        {
            if (string.IsNullOrEmpty(entrynumero.Text))
            {
                DisplayAlert("Advertencia", "Digite Numero de cedula", "ok");
            }
            else
            {
                int numeroC = int.Parse(entrynumero.Text);
                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    var person = conecction.Table<Personas>().FirstOrDefault(p => p.cedula == numeroC);
                    string n = null;
                    if (person != null)
                    {
                        conecction.Delete(person);
                        n = "encontrado";
                    }
                    if (n == null)
                    {
                        DisplayAlert("Advertencia", "El usuario fue Eliminado", "ok");
                    }
                }

                
            }
        }
    }
}