﻿using Parcial2Diego.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Parcial2Diego
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async public void Buttoncrear(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entrycedula.Text) || string.IsNullOrEmpty(entrynombres.Text) || string.IsNullOrEmpty(entryapellidos.Text))
            {
                 await DisplayAlert("Error", "Debe Escribir Datos completos", "Ok");
            }
            else
            {
                crearPersona();

                await Navigation.PushAsync(new Page1());

            }

        }

        public void crearPersona()
        {
            Personas person = new Personas()
            {

                cedula = int.Parse(entrycedula.Text),
                nombres = entrynombres.Text,
                apellidos = entryapellidos.Text,
                fechaNa = entryfecha.Date
            };


            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                connection.CreateTable<Personas>();


                var result = connection.Insert(person);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La persona se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La persona no fue creada", "OK");

                }

            }

            entrynombres.Text = "";
            entryapellidos.Text = "";
            entrycedula.Text = "";

        }
        

        async private void Buttonbuscar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

    }
}
